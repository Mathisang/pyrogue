import pygame
import math
import random


class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y, targetx, targety):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('assets/shot.png')
        self.rect = self.image.get_rect()
        self.rect.centery = y
        self.rect.centerx = x

        angle = math.atan2(targety - self.rect.centery, targetx - self.rect.centerx)

        self.dx = math.cos(angle)*random.randint(2, 5)
        self.dy = math.sin(angle)*random.randint(2, 5)

    def update(self):
        self.rect.y += int(self.dy)
        self.rect.x += int(self.dx)