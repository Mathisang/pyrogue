import pygame
from bullet import Bullet
from threading import Timer


class Ennemy(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super(Ennemy, self).__init__()

        self.image = pygame.image.load('assets/ennemy.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.life = 2
        self.invincible = False

    def shoot(self, all_sprites, bullets, playerX, playerY):
        bullet = Bullet(self.rect.centerx, self.rect.top, playerX, playerY)
        all_sprites.add(bullet)
        bullets.add(bullet)

    def removeLife(self):
        if not self.invincible:
            self.life = self.life - 1
            Timer(1.3, self.removeInvicible).start()
            self.invincible = True
            return self.life

    def removeInvicible(self):
        self.invincible = False