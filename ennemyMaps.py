import pygame
import random
from generateMap import generateMap
from ennemy import Ennemy
import maps


class ennemyMaps():
    def __init__(self, allEnemies):
        super().__init__()

        self.allEnemies = allEnemies

    def generate(self, numberEnnemies, level):
        for i in range(numberEnnemies[level]):
            valuey = 0
            valuex = 0

            while maps.getMaps(level)[valuey][valuex] != "-":
                ex = random.randint(0, 1080 - 32)
                ey = random.randint(0, 720 - 32)

                for i in range(0, 19):
                    if ey >= i * 36 and ey < (i + 1) * 36:
                        valuey = i

                    if ex >= i * 54 and ex < (i + 1) * 54:
                        valuex = i

            if maps.getMaps(level)[valuey][valuex + 1] == "X":
                ex -= 32

            if maps.getMaps(level)[valuey + 1][valuex] == "X":
                ey -= 32

            self.allEnemies.append(Ennemy(ex, ey))