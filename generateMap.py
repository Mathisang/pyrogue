import pygame
import maps


class generateMap():
    def __init__(self, screen, level):
        super().__init__()

        self.screen = screen
        self.screenx, self.screeny = screen.get_size()

        self.LEVEL = level
        self.NumberTiles = 20
        self.TileX = self.screenx / self.NumberTiles
        self.TileY = self.screeny / self.NumberTiles

    def generate(self, type, wall):
        self.map = maps.getMaps(self.LEVEL)

        for y, row in enumerate(self.map):
            for x, tileType in enumerate(row):
                if tileType == "X":
                    if type == "draw":
                        pygame.draw.rect(self.screen, (0, 0, 0),
                                         (x * self.TileX, y * self.TileY, self.TileX, self.TileY))
                    elif type == "clear":
                        wall = []
                    else:
                        wall.append(pygame.draw.rect(self.screen, (0, 0, 0), (x * self.TileX, y * self.TileY, self.TileX, self.TileY)))

    def getTilesSizes(self):
        return [self.TileX, self.TileY]