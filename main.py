import pygame
from player import Player
import random
from generateMap import generateMap
from ennemyMaps import ennemyMaps

pygame.init()

# Variables
LIFE = 5
LEVEL = 0
BACKGROUND = (255, 255, 255)

# Default settings
size = (1080, 720)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Roguelike")
x, y = screen.get_size()
clock = pygame.time.Clock()

loop = True

# Text health
font = pygame.font.SysFont(None, 24)
text = font.render("Vies : " + str(LIFE), True, (255, 0, 0))

# Generate first map
wall = []
generateMap(screen, LEVEL).generate("stock", wall)

# Generate player
mainPlayer = Player(200, 200)

# Generate ennemies
numberEnnemies = [3, 5, 7, 10]
allEnemies = []
ennemyMaps(allEnemies).generate(numberEnnemies, LEVEL)

# Add characters in sprite group
characters = pygame.sprite.Group()
characters.add(mainPlayer)
for i in range(numberEnnemies[LEVEL]):
    characters.add(allEnemies[i])

# Add bullets in sprite group
bullets = pygame.sprite.Group()

# SETTING TIMER
counterShot = 1
timer_event = pygame.USEREVENT + 1
pygame.time.set_timer(timer_event, 1000)

counterShotByEnnemy = 0
timerShot = pygame.USEREVENT + 1
pygame.time.set_timer(timerShot, 1000)

invincible = False
counterHit = 0

while loop:
    screen.fill(BACKGROUND)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            loop = False
        elif event.type == timer_event:
            counterShot -= 1
            if counterShot == 0:
                pygame.time.set_timer(timer_event, 1000)
                counterShot = 2
                for enn in allEnemies:
                    counterShotByEnnemy = random.randint(0, 1)
                    if event.type == timerShot:
                        counterShotByEnnemy -= 1
                        if counterShotByEnnemy == 0:
                            enn.shoot(characters, bullets, mainPlayer.rect.x, mainPlayer.rect.y)
        elif event.type == pygame.KEYDOWN:
            key = pygame.key.get_pressed()
            if key[pygame.K_SPACE]:
                mainPlayer.attack()

    if mainPlayer.hit:
        melee = pygame.draw.circle(screen, (212, 188, 152), (mainPlayer.rect.x + mainPlayer.width/2, mainPlayer.rect.y + mainPlayer.height/2), 40, 0)
        if melee.collidelist(allEnemies) >= 0:
            ennemyLife = allEnemies[melee.collidelist(allEnemies)].removeLife()
            if ennemyLife == 0:
                characters.remove(allEnemies[melee.collidelist(allEnemies)])
                del allEnemies[melee.collidelist(allEnemies)]
                numberEnnemies[LEVEL] -= 1

    if numberEnnemies[LEVEL] == 0:
        if LEVEL != 3:
            LEVEL += 1
            wall.clear()
            generateMap(screen, LEVEL).generate("stock", wall)
            mainPlayer.rect.x = x / 2
            mainPlayer.rect.y = y / 2
            ennemyMaps(allEnemies).generate(numberEnnemies, LEVEL)
            for i in range(numberEnnemies[LEVEL]):
                characters.add(allEnemies[i])
        else:
            print('WIN')

    generateMap(screen, LEVEL).generate("draw", wall)

    hits = pygame.sprite.spritecollide(mainPlayer, bullets, False)
    if hits:
        shotted = mainPlayer.shotBy(hits)

        if shotted == "loose life":
            LIFE -= 1
            text = font.render("Vies : " + str(LIFE), True, (255, 0, 0))
            screen.blit(text, (20, 20))

    if LIFE == 0:
        print('GAME OVER')
        exit()

    mainPlayer.moving(wall)

    characters.update()
    characters.draw(screen)

    screen.blit(text, (20, 20))
    pygame.display.flip()

    clock.tick(60)

pygame.quit()
