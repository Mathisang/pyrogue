import pygame
from threading import Timer


class Player(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super(Player, self).__init__()

        self.image = pygame.image.load('assets/player.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.vx = 0
        self.vy = 0
        self.height = 32
        self.width = 32
        self.attacking = False
        self.hit = False
        self.invincible = False

    def moving(self, walls):
        self.vx = 0
        self.vy = 0
        key = pygame.key.get_pressed()

        MOVESPEED = 5

        if key[pygame.K_LEFT]:
            self.rect.x += -MOVESPEED
            for wall in walls:
                if self.rect.y + self.height > wall.y and self.rect.y < wall.y + wall.height and self.rect.x + self.width > wall.x and self.rect.x < wall.x + wall.width:
                    self.rect.x = wall.x + wall.width
        if key[pygame.K_RIGHT]:
            self.rect.x += MOVESPEED
            for wall in walls:
                if self.rect.y + self.height > wall.y and self.rect.y < wall.y + wall.height and self.rect.x + self.width > wall.x and self.rect.x < wall.x + wall.width:
                    self.rect.x = wall.x - self.width
        if key[pygame.K_UP]:
            self.rect.y += -MOVESPEED
            for wall in walls:
                if self.rect.y + self.height > wall.y and self.rect.y < wall.y + wall.height and self.rect.x + self.width > wall.x and self.rect.x < wall.x + wall.width:
                    self.rect.y = wall.y + wall.height
        if key[pygame.K_DOWN]:
            self.rect.y += MOVESPEED
            for wall in walls:
                if self.rect.y + self.height > wall.y and self.rect.y < wall.y + wall.height and self.rect.x + self.width > wall.x and self.rect.x < wall.x + wall.width:
                    self.rect.y = wall.y - self.height

    def attack(self):
        if not self.attacking:
            self.attacking = True
            self.hit = True

            Timer(0.2, self.hitRemove).start()
            Timer(0.9, self.waiting).start()

            return self.attacking

    def waiting(self):
        self.attacking = False

    def hitRemove(self):
        self.hit = False

    def shotBy(self, hit):
        if hit:
            if not self.invincible:
                self.invincible = True
                Timer(1.8, self.removeInvincible).start()
                return "loose life"

    def removeInvincible(self):
        self.invincible = False